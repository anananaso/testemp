<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/fpart', 'FPartController@index');
Route::get('/part1', 'EmployeesController@index');
Route::get('/part2', 'EmployeesController@show')->middleware('auth');
Route::get('/part2/{sort}', 'EmployeesController@show')->middleware('auth');

