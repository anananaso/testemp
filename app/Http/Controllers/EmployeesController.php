<?php

namespace App\Http\Controllers;
use App\Employees;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class EmployeesController extends Controller
{
    public function index(Employees $employees){
        $rootEmployees = $employees->rootEmployees();
        return view('part1', ['rootEmployees' => $rootEmployees]);
    }

    public function show($sort = 'id') {
        $employees = Employees::orderBy($sort, 'asc')->paginate(10);
        return view('part2', compact('employees'));
    }


}
