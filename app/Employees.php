<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    public $timestamps = false;

    protected $table = 'employees';

    public function Employees(){
        return $this->hasMany($this, 'id_chief');
    }

    public function rootEmployees(){
        return $this->where('id_chief', 0)->get();
    }

    public function sortEmployees($sort){
        return $this->orderBy($sort, 'desc')->get();
    }

}
