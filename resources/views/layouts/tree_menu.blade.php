<ul>
    @foreach($employees as $employee)
        <li>
            Name : {{ $employee->name }}</br>
            Position : {{ $employee->position }}</br>
            Salary : {{ $employee->salary }}</br>
            Hire date : {{ $employee->hire_date }}
        </li>
        @if($employee->Employees->count() > 0)
            @include('layouts.tree_menu', ['employees' => $employee->Employees])
        @endif
    @endforeach
</ul>
