@extends('layouts.app')
@section('content')
{{--<h1>Employees</h1>--}}
{{--<ul>--}}
    {{--@foreach($rootEmployees as $rootEmployee)--}}
        {{--<li>{{ $rootEmployee->name }}</li>--}}
        {{--@if($rootEmployee->Employees->count() > 0)--}}
            {{--@include('layouts.tree_menu', ['employees' => $rootEmployee->Employees])--}}
        {{--@endif--}}
    {{--@endforeach--}}
{{--</ul>--}}

<h1>Employees</h1>
<div class="accordion" id="accordionExample">
    @foreach($rootEmployees as $rootEmployee)
        <div class="card">
        <div class="card-header" id="heading{{ $rootEmployee->id }}">
                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{ $rootEmployee->id }}" aria-expanded="false" aria-controls="{{ $rootEmployee->id }}">
                    Name : {{ $rootEmployee->name }}</br>
                    Position : {{ $rootEmployee->position }}</br>
                    Salary : {{ $rootEmployee->salary }}</br>
                    Hire date : {{ $rootEmployee->hire_date }}
                </button>
        </div>
            @if($rootEmployee->Employees->count() > 0)
            <div id="collapse{{ $rootEmployee->id }}" class="collapse" aria-labelledby="heading{{ $rootEmployee->id }}" data-parent="#accordionExample">
                    <div class="card-body">
            @include('layouts.tree_menu', ['employees' => $rootEmployee->Employees])
                    </div>
                </div>
        @endif


        </div>
    @endforeach
</div>

@endsection

