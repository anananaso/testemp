@extends('layouts.app')
@section('content')
    <div class="btn-group">
        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Sorting by
        </button>
        <div class="dropdown-menu dropdown-menu-right">
            <button class="dropdown-item" type="button"><a href='{{ url('/part2',['sort'=>'id'])}}'>Id</a></button>
            <button class="dropdown-item" type="button"><a href='{{ url('/part2',['sort'=>'id_chief'])}}'>Chief #</a></button>
            <button class="dropdown-item" type="button"><a href='{{ url('/part2',['sort'=>'name'])}}'>Name</a></button>
            <button class="dropdown-item" type="button"><a href='{{ url('/part2',['sort'=>'position'])}}'>Position</a></button>
            <button class="dropdown-item" type="button"><a href='{{ url('/part2',['sort'=>'salary'])}}'>Salary</a></button>
            <button class="dropdown-item" type="button"><a href='{{ url('/part2',['sort'=>'hire_date'])}}'>Hire date</a></button>
        </div>
    </div>

    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Chief #</th>
            <th scope="col">Name</th>
            <th scope="col">Position</th>
            <th scope="col">Salary</th>
            <th scope="col">Hire date</th>
        </tr>
        </thead>
        <tbody>
        @foreach($employees as $employee)
            <tr>
                <td>{{$employee->id}}</td>
                <td>{{$employee->id_chief}}</td>
                <td>{{$employee->name}}</td>
                <td>{{$employee->position}}</td>
                <td>{{$employee->salary}}</td>
                <td>{{$employee->hire_date}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{$employees->links()}}
@endsection
